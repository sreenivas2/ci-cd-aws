#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo

# clone the repo again

echo "Cloning repo"

cd /var/SkillMonks_Repo/skillmonks

## pull the latest code


#build

mvn install:install-file "-Dfile=/var/SkillMonks_Repo/skillmonks/PaymentGatewayJars/SingleUrlDependency-1.0.12.jar" "-DgroupId=tp.util" "-DartifactId=SingleUrlDependency" "-Dversion=1.0.12" "-Dpackaging=jar"

mvn install:install-file "-Dfile=/var/SkillMonks_Repo/skillmonks/PaymentGatewayJars/checksum-1.2.jar" "-DgroupId=checksum"
mvn -Pprod package -DskipTests

echo "Build success"



# rename and copying war file


